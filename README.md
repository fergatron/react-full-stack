# react-full-stack
A simple boilerplate of a React.Js application that's both the server and the client. The tech stack used is React, Node.js, Express and Webpack. This project is based on Sandeep Raveesh's Hackernoon article.

## Installation
```bash
npm install --save express react react-dom
npm install --save-dev babel-core babel-loader babel-preset-env babel-preset-react clean-webpack-plugin concurrently css-loader eslint eslint-config-airbnb eslint-plugin-react html-webpack-plugin nodemon style-loader webpack webpack-cli webpack-dev-server
```

## Usage
```bash
npm run build - builds production-ready files
npm run client - starts dev server
npm run dev - runs both client/server
npm run server - starts express server
npm run start - starts express server in production
```

## Dev notes
* **eslint-plugin-jsx-a11y** is used in conjunction with Visual Studio Code and the ESLint plugin.

## References
1. https://github.com/crsandeep/simple-react-full-stack
1. https://hackernoon.com/full-stack-web-application-using-react-node-js-express-and-webpack-97dbd5b9d708